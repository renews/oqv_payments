module OqvPayments
  # Configuration
  # ===
  # This class is used to configure OqvPayments defaults
  class Configuration
    # Configures the adyen endpoint for making authorization requests
    attr_accessor :auth_url

    # Configures the adyen endpoint for making refund requests
    attr_accessor :refund_url

    # Configures the adyen and point for making capture requests
    attr_accessor :capture_url

    # Configures the adyen and point for making cancel requests
    attr_accessor :cancel_url

    # Configures the endpoint used to listing cards
    attr_accessor :list_cards_url

    # Configures the endpoint used to disable cards
    attr_accessor :disable_cards_url

    # Configures the basic-auth user
    attr_accessor :auth_user

    # Configures the basic-auth password
    attr_accessor :auth_pass

    # Configures the merchant account used for transactions
    attr_accessor :merchant_account

    # Configures the contract type
    attr_accessor :contract

    # Configures the number of retries used in the capture operation
    attr_accessor :capture_retry_count

    # Configures the number of retries used in the cancel operation
    attr_accessor :cancel_retry_count

    def initialize
      @auth_url = 'https://pal-test.adyen.com/pal/servlet/Payment/v12/authorise'
      @refund_url = 'https://pal-test.adyen.com/pal/servlet/Payment/v12/refund'
      @capture_url = 'https://pal-test.adyen.com/pal/servlet/Payment/v12/capture'
      @cancel_url = 'https://pal-test.adyen.com/pal/servlet/Payment/v12/cancel'
      @list_cards_url = 'https://pal-test.adyen.com/pal/servlet/Recurring/v12/listRecurringDetails'
      @disable_cards_url = 'https://pal-test.adyen.com/pal/servlet/Recurring/v12/disable'
      @auth_user = 'ws@Company.OQVestir'
      @auth_pass = '67X][YtCcHPeE<fe*YpU11LcX'
      @merchant_account = 'OQVestirCOM'
      @contract = 'ONECLICK'
      @capture_retry_count = 2
      @cancel_retry_count = 2
    end

    def [](option)
      __send__(option)
    end
  end
end
