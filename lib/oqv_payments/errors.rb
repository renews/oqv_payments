module OqvPayments
  # = OqvPayments Errors
  #
  # Generic exception class.
  class OqvPaymentError < StandardError
    attr_reader :request

    def initialize(msg = nil, request = nil)
      super(msg)
      @request = request
    end
  end

  # Raised when a request returns a 401 status(most likely missing authorization data)
  class UnauthorizedRequestError < OqvPaymentError
  end

  # Raised when a request returns a 422
  class UnprocessableEntityError < OqvPaymentError
  end

  # Raised when the capture request returns a failure
  class CaptureError < OqvPaymentError
  end

  # Raised when the capture retry count is exhausted
  class CaptureRetryExceededError < OqvPaymentError
  end

  # Raised when the Cancel request returns a failure
  class CancelError < OqvPaymentError
  end

  # Raised when the Cancel retry count is exhausted
  class CancelRetryExceededError < OqvPaymentError
  end

  # Raised when the Cancel request returns a sucess
  class CancelSuccessError < OqvPaymentError
  end
end
