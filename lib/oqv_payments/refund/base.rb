module OqvPayments
  module Refund
    class Base
      attr_reader :last_error
    
      def valid?
        true
      end

      def dispatch
        raise OqvPayments::OqvPaymentError.new('Not implemented')
      end
    end
  end
end
