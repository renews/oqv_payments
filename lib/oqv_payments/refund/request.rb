module OqvPayments
  module Refund
    # = OqvPayments Refund
    #
    # Used to generate refund requests to adyen
    class Request < Base
      include OqvPayments::AttributeSerializer

      ATTRIBUTES = [
        :merchant_account,
        :modification_amount,
        :original_reference,
        :reference,
        :value,
        :currency
      ]

      def initialize(*args)
        options = args.extract_options!
        merchant =  options[:merchant_account] || OqvPayments.config.merchant_account

        @reference = options[:reference]
        @original_reference = options[:original_reference]
        @merchant_account = merchant
        @modification_amount = Attribute::ModificationAmount.new(options[:modification_amount])
        @value = (options[:value] * 100).to_i.to_s rescue nil
        @currency = options[:currency]
      end

      def valid?
        unless @merchant_account.present?
          @last_error = 'Faltando configuração "merchant_account"'
          return false
        end

        unless self.as_json.values.all? { |x| !x.nil? }
          @last_error = 'Nenhuma informação foi enviada'
          return false
        end

        unless @modification_amount.valid?
          @last_error = 'Valor do estorno inválido'
          return false
        end

        true
      end

      def dispatch
        unless valid?
          raise OqvPayments::OqvPaymentError.new(last_error)
        end

        request_params = self.as_json.reject { |k, v| v == nil }
        auth = {
          user: OqvPayments.config.auth_user,
          pass: OqvPayments.config.auth_pass
        }

        response = HTTP.basic_auth(auth).post(OqvPayments.config.refund_url, json: request_params)
        json = response.parse rescue nil

        if response.status == 401 || response.status == 403
          raise OqvPayments::UnauthorizedRequestError.new
        elsif response.status == 422
          raise OqvPayments::UnprocessableEntityError.new(json['message'], json)
        elsif response.status == 200
          json.merge!({'status' => response.status.code})
        else
          raise OqvPayments::OqvPaymentError.new(json['message'], json)
        end
      end
    end
  end
end
