module OqvPayments
  module Request
    module Attribute
      class Auth < Base
        include OqvPayments::AttributeSerializer

        # Attributes to be searialized to adyen
        ATTRIBUTES = [
          :user,
          :pass
        ].freeze

        attr_accessor *ATTRIBUTES

        def initialize(*args)
          options = args.extract_options!

          @user = options[:user]
          @pass = options[:pass]
        end

        def valid?
          return false unless @user.present?
          return false unless @pass.present?

          true
        end
      end
    end
  end
end
