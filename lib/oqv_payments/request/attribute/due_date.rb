module OqvPayments
  module Request
    module Attribute
      class DueDate < Base

        # Attributes to be searialized to adyen
        ATTRIBUTES = [:date].freeze

        attr_accessor *ATTRIBUTES

        def initialize(paper_days)
          @date = paper_days.to_i.business_days.from_now
        end

        def valid?
          return false unless @date.present?
          return false unless @date.class == Time

          true
        end

      end
    end
  end
end