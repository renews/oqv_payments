module OqvPayments
  module Request
    module Attribute
      # GenericAddress
      # ===
      # Generic class used for address fields
      class GenericAddress < Base
        include OqvPayments::AttributeSerializer

        # Attributes to be searialized to adyen
        ATTRIBUTES = [
          :street,
          :house_number_or_name,
          :city,
          :postal_code,
          :state_or_province,
          :country
        ].freeze

        attr_accessor *ATTRIBUTES

        def initialize(*args)
          options = args.extract_options!

          @street = options[:street]
          @house_number_or_name = options[:house_number_or_name]
          @city = options[:city]
          @postal_code = options[:postal_code]
          @state_or_province = options[:state_or_province]
          @country = options[:country]
        end

        def valid?
          return false unless @street.present?
          return false unless @house_number_or_name.present?
          return false unless @city.present?
          return false unless @postal_code.present?
          return false unless @country.present?

          true
        end
      end
    end
  end
end
