module OqvPayments
  module Request
    module Attribute
      class Installment < Base
        include OqvPayments::AttributeSerializer

        # Attributes to be searialized to adyen
        ATTRIBUTES = [
          :value
        ].freeze

        attr_accessor *ATTRIBUTES

        def initialize(*args)
          options = args.extract_options!

          @value = options[:value].to_i
        end

        def valid?
          return false unless @value > 0 && @value <= 12

          true
        end
      end
    end
  end
end
