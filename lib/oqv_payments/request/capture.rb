module OqvPayments
  module Request
    # Capture
    # ===
    # This class is used to make a capture from an authorised request
    #
    # Creation
    # ---
    # ```ruby
    # request = OqvPayments::Request::Capture.new(
    #   {
    #     modification_amount: {
    #       value: "100", # 100 is equivalent of 1,00 BRL, see adyen docs for different currency, the value of the capture
    #       currency: "BRL"
    #     },
    #     original_reference: "34612836418723462", # The original pspReference from authorisation
    #     reference: "pada1", # A reference for this request(normally the order_number)
    #     value: "2990", # The value of the capture
    #     currency: "BRL"
    #   }
    # )
    # ```
    class Capture < Base
      include OqvPayments::AttributeSerializer

      # Attributes to be searialized to adyen
      ATTRIBUTES = [
        :merchant_account,
        :modification_amount,
        :original_reference,
        :reference,
        :value,
        :currency
      ]

      def initialize(*args)
        options = args.extract_options!
        merchant =  options[:merchant_account] || OqvPayments.config.merchant_account
        @reference = options[:reference]
        @original_reference = options[:original_reference]
        @merchant_account = merchant
        @modification_amount = Attribute::ModificationAmount.new(options[:modification_amount])
        @value = (options[:value] * 100).to_i.to_s rescue nil
        @currency = options[:currency]
      end

      # Validates the current capture request data
      def valid?
        unless @merchant_account.present?
          @last_error = 'Faltando configuração "merchant_account"'
          return false
        end

        unless @original_reference
          @last_error = 'Referência original não especificada'
          return false
        end

        unless @reference
          @last_error = 'Referência não especificada'
          return false
        end

        unless @modification_amount.valid?
          @last_error = 'Valor de captura inválido'
          return false
        end

        true
      end

      def dispatch
        unless valid?
          raise OqvPayments::CaptureError.new(last_error)
        end

        retries = 0

        while true
          if retries >= OqvPayments.config.capture_retry_count
            raise OqvPayments::CaptureRetryExceededError
          end

          request_params = self.as_json.reject { |k, v| v == nil }

          response = HTTP.basic_auth(auth_adyen).post(OqvPayments.config.capture_url, json: request_params)
          json = response.parse rescue nil

          if response.status == 401 || response.status == 403
            raise OqvPayments::UnauthorizedRequestError.new
          elsif response.status == 422
            raise OqvPayments::UnprocessableEntityError.new(json['message'], json)
          elsif response.status == 200
            if json['response'] == '[capture-received]'
              return json
            else
              raise OqvPayments::CaptureError.new(json['response'], request_params)
            end
          end
        end
      end
    end
  end
end
