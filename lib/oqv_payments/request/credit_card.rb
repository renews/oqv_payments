module OqvPayments
  module Request
    # CreditCard
    # ===
    # This class is used to make a credit card request to adyen
    #
    # Creation
    # ---
    # ### Non-encrypted
    # ```ruby
    # request = OqvPayments::Request::CreditCard.new(
    #   {
    #     card: {
    #       number: "4111111111111111",
    #       expiry_month: "6",
    #       expiry_year: "2016",
    #       cvc: "737",
    #       holder_name: "teste ti"
    #     },
    #     amount: {
    #       value: "100", # 100 is equivalent of 1,00 BRL, see adyen docs for different currency
    #       currency: "BRL"
    #     },
    #     reference: "teste",
    #     shopper_email: "ivan.birkman@oqvestir.com.br", # These last 3 fields are only sent if you wish to save the payment details on adyen's base
    #     shopper_reference: "pada1", # Unique identifier which will be used to list/use saved payment details
    #     recurring: {
    #       contract: "RECURRING"
    #     }
    #   }
    # )
    # ```
    #
    # ### Using Client Side Encryption
    # ```ruby
    # request = OqvPayments::Request::CreditCard.new(
    #     {
    #       amount: {
    #         value: "100",
    #         currency: "BRL"
    #       },
    #       # With Easy Encryption no card object has to be passed, only the aditional data with the card's hash
    #       additional_data: {
    #         card_encrypted_json: "adyenjs_0_1_17$TAC2MFCv0xMdGbupzcWAaf0yvFT/CgTCNhYGxapJKfwT3acii63zeqyRkx9hX8S2PDCBhbN+eaowzF07lXOyQJdx5TDI7H23KMBxiAgBJvIDJhtxRNInrILtE+SMjzgYOAql+MxGd3zWWf5qjTxRBtBs4FlflfQe9QTX0r+cy6NH6ArphuXe6vkEjLXTExjHjLNXDqlainMheg0pq5cRJtLgUaalra4gGkWxKXBv6xdA3KAzIr73W0Zr/H5O8MxUa4GTji/e4mQq5thT0Cnbwwk7z7tHMeIuEECvjS5l1m6zPVqgDRwplU+NR0Bo4WAV9b5eqFe6cHVeJjArx9seHw==$MeiN4Dv9B6ZCaPufjiprbUClv9ucZSWslJ1uNieuitAimhp178GrX9WnI2m/X10xmG3v7iCUofZDdOG2F5l//hGM5Gtm1qIFtPDQ0g2539+/chogf/H8jKyatGwcr/9kHfJk0QwzC6LZwsnf2rwem1GyHbWpIOdNizsZmJQUys6qjYeR+wu+lFtKloA1n8n0dctXZrQRq5uyZIsBa1Sg26egVSETfmVWCEF2HM31jqS0Oq9KAxb0SJsWJ3PCzZWZtcSm+8HAr48lrBBHgvP+tQOq7RpUEBbHkRhkqntpszDEbPzxMSt3C71hcpGDwmfscRiBNYZv7yQbjdLV30EXp0DdOZKH/L1c9sE9b0+XZr4rtcSe/5XnIwVRorrwTnRkExW+nVxxpysbIjSWZVZBplVRv9zI+LufVUUF+hF//7DeurdbIuu42L3UwXz/nHWoTfY3wZ9dtZEQDGz4a2w8mJ/+HOdAFTEHcA7Keez65qvCaIHJ0UHnd2dzdOHaJQoWkFZ9odM/mZqm6CCfeije8Mh0l2cOG3hQl/3ucxu55ZmNdIEiEKwPL0Fv2Adq7nEd6JTxMpSrBCLh8VVIGylma4fw8ixc77uom8FhD0cuVHzrwlj6OjdP0ANSX0gJHwum3xpqmvNSLwLMCPu8Qgfu85V6ATjSxNNacLE8yC3NJhj0IT34G84Z5O9G3BfksjD/Ky2RiM20pgA47K21GlEWUhLSMJU+M9zUYe1g5um1Tn9l3ZAMYj2YnRaKpmqqG148djIRd/TPXAsk84+00g3QucwBkRcLQtsYuNIXsp75Y7PUGMM54BYLa8GIwAD6rsO5ISYqiPMyxAF3DX+FNDuXfExbAig1DMd9Lv+CcHqI22tCoLXzWqTsEhdsLVvdjpFk/A3z4htrZQ1qogm3VsonkuODeJPJf9+Yxy00P8p03JEYvQ587oofZ1HGJUUcZaizBURc4K69jvqFBNAmLRVraNXhw6vPraF/SHayJs6MQfNC76Zkfhd5DVr3ICE7MKeMUHk2tvt6lIiP3G0AxJQEBRVbQJQ6GAzVZwKFqA8CN71KO9vnJN/bd+y1elIrc8f4tAZGTdCX6l0vW2UDQR9J73umtOHfgwuvoGZYgtlBnmUFGl3fTII88T9fmgvxmZmXXKNPNaVxdhiYmNuxvZ6GguK4uZtPS7iYvicSc0/vSS16WuaR/WOM6MBzw//gGzqGMe29upGCN89o94LI7V7ZGlSWqJo9Ofcx1IroBX1l9FFhCa3y5aMuzFsXjKEPQxTO9NQthrlgLAhUUauBn9CcbYc6zTT9LJoIN7Fq8qKBr0k/oc5plpJV1atr/2g0qfs0bLTICm7PbknfGgFNC2nzadpf+XiKvMgzhoHkGSa0NPRDa/XRXszRV8JTN2bHt0ghbzkYsT0i9aF7QI7LiCUxQk2gJRTBt0Y+trV2pla8tjOXfts92xcgRvsK5IgZJEEw5qtAS79VjIzrW32SYgJIGgJKQeRDgGO4yIiMOIXnFj96ZOYYc3mkPNdRmLiZMEqu8G19eo3vZLF8mUDgaQPFciPCe+4zgb3bEPFNdfDpdRPmFXhwM9MyoBij2Qh7lnyohLkQ6kK4EVdCfh2aJL3wWgW/FQgCm00j6VwDuvHxwTJQMLpk+kOg1WDlA9z6sg7S3YaHtYXFcO/CiC6kZdMv3XLdTOAoj4HKAxD6uZgIrJvCg4ozIFXpO9DaqEO4XX6K/a3AX9uCodC8x+HiUTyTOtWHYDLymYAg4IoNQewWXFQA2mcunPqkg1QSQZYa0y+ZzDhuYRenvvRIElV6OU1VHRZFqVG3KHa7owzXl/EAlvK2Hrw0bxw3LfbxszwAQ0NNircP8frqxeikBr0+twOhxRmwuZ3hnBGBT2iSPcFNoxo8wC76/aLcYfLfLoaLGu2WXaIpudJIh/1N1ZhI9vb0tRR7vm89IYQHtDz/ULYRPpaj+XRXVtafIHZOnmjVKrplHKl1GbEOx8OZNHi1T2Xi/Ygf8D0KYI5jnEn9j9pwKnptA2HEMS4bJ+Q1BZK074BG9apPfN6ul33tYtHjRWpiTBIRFy8a7PEwq/oPtjB0F4BVgIM8MyE38boTBuKBwLHycxCdHEM6vQ9ssVVIAeV2lopZCXmJSgIPoGtcQpvym6r//oifOaDcS+v6zAqvzenmsDKxVmsctZTMzPDwWOTtRHmOhyra99cqKLSyLh8gguE/x4MZbUS+l3Tqt37Y1FlEl/yXq8XZ/YT/TKQbDTTI2jNLuNg4YYhdgmurulEeUnuq/Fbe/qRL3CL2jId7PDyrcTJ0e6dDAaCfNe4AoXDWs+0AzazsS6zLvaBWd3xaYFzp0/+KY+ow2rleW2BoUxy92opfNwk6xXQBuqKw/ywfB+FZ0284E8EmJLNiIUbdBcyh2PCc77Qy4kkqRj6N33do2bPpTPc23tpuTCzl2nA1urQRFFOlMsZqFLaulEgFblrNdABiuclzvcjn2arqPYC7kbJvDzjPXAcmbhBbTJFO91wfVlkkFWCgMhICl7CkW513g7C7Fx6tOcz2zP8pzy9NtVeQOkDLkbfugn2vqVxVrVDa5ZAKowPbe4jYWYNY89YDIQ7iGijA/QkOJlnJuMddMbJdU/ivRcxxT8vxSqqkBaZHsrJqGWqF/7lARPmyyFoTHI0kpM1k44TcqZ0Dj5754wyDrY+qMID3QQpUlLD5CwKGA1Cc9EGg0fbnY1c67pEPUYqQZx6nd81EfAbwwtkoe2N6GXj+pNlmY319B1Eyxz7gZhfCWEyUGzUpocEM/xcq5v9BDj0Edu6wjlziIm/nvyMcPXPHV9xp8S45Z5dOsMH2ld2Td3xhUijoMqq5DmJArcoHqSiRcqJLlvp1hE4EK1/W9PRBF9wdu9fIvis32n6Z4921lsuhZd+n93xCP3vfb1tZcScGy96evE5dz41ISde6/imkiGKo6xCfrAAmHtl/dJ5WpKk1NFrcMf9N2gZIe+1grSpGnrEPfqzEJaKZmt4pY4/+j9PlQrs6hpN56pJ42Riry8uj+fx3O+0q0TlOkw0dEsg9oNW0XHTe"
    #       },
    #       # Installments should be a positive value and only passed when actually splitting the payment
    #       installments: {
    #        value: 3
    #       },
    #       reference: "teste",
    #       shopper_email: "ivan.birkman@oqvestir.com.br",
    #       shopper_reference: "pada1",
    #       recurring: {
    #         contract: "RECURRING"
    #       }
    #     }
    # )
    # ```
    #
    # Making a payment
    # ---
    #
    # You can call the dispatch method after setting up the request data.
    # if the request fails it will throw an exception related to the kind of error,
    # if you want to catch the error independent of the kind, you must rescue the
    # OqvPaymentError exception
    class CreditCard < Base
      include OqvPayments::AttributeSerializer

      # Attributes to be searialized to adyen
      ATTRIBUTES = [
        :amount,
        :reference,
        :merchant_account,
        :shopper_email,
        :shopper_reference,
        :additional_data,
        :card,
        :recurring,
        :installments,
        :selected_recurring_detail_reference,
        :delivery_address,
        :billing_address,
        :social_security_number,
        :delivery_date,
        :shopper_name,
        :date_of_birth_day_of_month,
        :date_of_birth_month,
        :date_of_birth_year,
        :shopper_ip
      ].freeze

      attr_accessor *ATTRIBUTES

      def initialize(*args)
        options = args.extract_options!
        merchant =  options[:merchant_account] || OqvPayments.config.merchant_account

        @amount = Attribute::Amount.new(options[:amount])
        @card = Attribute::Card.new(options[:card]) if options[:card].present?
        @additional_data = Attribute::AdditionalData.new(options[:additional_data]) if options[:additional_data].present?
        @recurring = Attribute::Recurring.new(options[:recurring]) if options[:recurring].present?
        @installments = Attribute::Installment.new(options[:installments]) if options[:installments].present?
        @merchant_account = merchant
        @reference = options[:reference]
        @shopper_reference = options[:shopper_reference]
        @shopper_email = options[:shopper_email]
        @shopper_ip = options[:shopper_ip]
        @selected_recurring_detail_reference = options[:selected_recurring_detail_reference]
        @social_security_number = options[:social_security_number]
        # Risk system attributes are optional
        @delivery_address = Attribute::DeliveryAddress.new(options[:delivery_address]) if options[:delivery_address].present?
        @billing_address = Attribute::BillingAddress.new(options[:billing_address]) if options[:billing_address].present?
        @shopper_name = Attribute::ShopperName.new(options[:shopper]) if options[:shopper].present?
        @delivery_date = options[:delivery_time].to_i.business_days.from_now if options[:delivery_time].present?
        if options[:birthdate].present?
          options[:birthdate] = Date.parse(options[:birthdate]) unless options[:birthdate].is_a? Date
          @date_of_birth_day_of_month = options[:birthdate].day if options[:birthdate].day.present?
          @date_of_birth_month = options[:birthdate].month if options[:birthdate].month.present?
          @date_of_birth_year = options[:birthdate].year if options[:birthdate].year.present?
        end
      end

      # Validates the current credit card request data
      def valid?
        unless @merchant_account.present?
          @last_error = 'Faltando configuração "merchant_account"'
          return false
        end

        unless @amount.valid?
          @last_error = 'Valor inválido'
          return false
        end

        unless @recurring.nil?
          unless @recurring.is_a? Attribute::Recurring
            @last_error = 'Configuração para salvar dados de cartão(recurring) no formato incorreto'
            return false
          end

          unless @recurring.valid?
            @last_error = 'Configuração para salvar dados de cartão(recurring) é inválida'
            return false
          end
        end

        unless @installments.nil?
          unless @installments.is_a? Attribute::Installment
            @last_error = 'Parcelamento em formato inválido'
            return false
          end

          unless @installments.valid?
            @last_error = 'Parcelamento inválido'
            return false
          end
        end

        unless @additional_data.nil?
           unless @additional_data.is_a? Attribute::AdditionalData
            @last_error = 'Dados adicionais no formato incorreto'
            return false
           end

          unless @additional_data.valid?
            @last_error = 'Dados adicionais inválidos'
            return false
          end
        end
        unless @card.nil?
          unless @card.is_a? Attribute::Card
            @last_error = 'Cartão inválido'
            return false
          end

          unless @card.valid?
            @last_error = 'Dados do cartão inválido'
            return false
          end
        end

        true
      end

      def dispatch
        json = super

        unless json['additionalData']['fraudResultType'] == 'AMBER'
          capture = OqvPayments::Request::Capture.new modification_amount: { value: @amount.value.to_f / 100, currency: 'BRL' },
                                                      original_reference: json['pspReference'],
                                                      reference: @reference,
                                                      merchant_account: @merchant_account

          begin
            capture.dispatch
          rescue OqvPayments::CaptureError, OqvPayments::CaptureRetryExceededError
            cancel = OqvPayments::Request::Cancel.new original_reference: json['pspReference'],
                                                      reference: @reference,
                                                      merchant_account: @merchant_account
            cancel.dispatch

            raise OqvPayments::CancelSuccessError
          end
        end

        json
      end
    end
  end
end
