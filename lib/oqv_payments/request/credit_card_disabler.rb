module OqvPayments
  module Request

    class CreditCardDisabler < Base
      include OqvPayments::AttributeSerializer

      # Attributes to be searialized to adyen
      ATTRIBUTES = [
        :merchant_account,
        :shopper_reference,
        :selected_recurring_detail_reference
      ].freeze

      attr_accessor *ATTRIBUTES

      def initialize(*args)
        options = args.extract_options!
        merchant =  options[:merchant_account] || OqvPayments.config.merchant_account

        @merchant_account = merchant
        @shopper_reference = options[:shopper_reference] if options[:shopper_reference].present?
        @selected_recurring_detail_reference = options[:selected_recurring_detail_reference] if options[:selected_recurring_detail_reference].present?
      end

      # Validates the current credit card request data
      def valid?
        unless @merchant_account.present?
          @last_error = 'Faltando configuração "merchant_account"'
          return false
        end

        unless @shopper_reference.present?
          @last_error = 'Faltando referência do comprador'
          return false
        end

        unless @selected_recurring_detail_reference.present?
          @last_error = 'Faltando token do cartão'
          return false
        end

        true
      end

      def disable_cards
        body_js = {
          "merchantAccount": @merchant_account,
          "shopperReference": @shopper_reference,
          "recurringDetailReference": @selected_recurring_detail_reference
        }
        response = HTTP.basic_auth(auth_adyen).post(OqvPayments.config.disable_cards_url, json: body_js)
        response
      end
    end
  end
end
