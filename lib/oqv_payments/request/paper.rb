module OqvPayments
  module Request
    # Paper
    # ===
    # This class is used to make a 'boleto bancário' request to adyen
    #
    # Creation
    # ---
    # ```ruby
    # request = OqvPayments::Request::Paper.new(
    #   {
    #     amount: {
    #       value: "100", # 100 is equivalent of 1,00 BRL, see adyen docs for different currency
    #       currency: "BRL"
    #     },
    #     billing_address: {
    #       city: "São Paulo",
    #       country: "BR",
    #       house_number_or_name: "999",
    #       postal_code: "04787910",
    #       state_or_province: "SP",
    #       street: "Roque Petroni Jr"
    #     },
    #     shopper_name: {
    #       first_name: "José",
    #       last_name: "Silva"
    #     },
    #     delivery_date: "3", # ISO 8601 date format, X days from time of creation
    #     reference: "Teste Boleto",
    #     selected_brand: "boletobancario_bradesco",
    #     shopper_statement: "Aceitar o pagamento até 15 dias após o vencimento.&#xA;Não cobrar juros. Não aceitar o pagamento com cheque",
    #     social_security_number: "12121212121"
    #   }
    # )
    # ```
    # Creating a paper request will instantiate the Attribute::BillingAddress, Attribute::ShopperName, Attribute::Amount and Attribute::DueDate classes.
    #
    # Making a payment
    # ---
    #
    # You can call the dispatch method after setting up the request data.
    # if the request fails it will throw an exception related to the kind of error,
    # if you want to catch the error independent of the kind, you must rescue the
    # OqvPaymentError exception
    class Paper < Base
      include OqvPayments::AttributeSerializer

      # Attributes to be searialized to adyen
      ATTRIBUTES = [
        :amount,
        :billing_address,
        :delivery_date,
        :reference,
        :merchant_account,
        :selected_brand,
        :shopper_name,
        :shopper_statement,
        :social_security_number,
      ].freeze

      attr_accessor *ATTRIBUTES

      def initialize(*args)
        options = args.extract_options!
        merchant =  options[:merchant_account] || OqvPayments.config.merchant_account

        @amount = Attribute::Amount.new(options[:amount])
        @billing_address = Attribute::BillingAddress.new(options[:billing_address])
        @shopper_name = Attribute::ShopperName.new(options[:shopper_name])
        @social_security_number = options[:social_security_number]
        @shopper_statement = options[:shopper_statement]
        @selected_brand = options[:selected_brand]
        @reference = options[:reference]
        @delivery_date = Attribute::DueDate.new(options[:delivery_date]).date.xmlschema(fraction_digits=3)
        @merchant_account = merchant
      end

      def valid?
        unless self.as_json.values.all? { |x| !x.nil? }
          @last_error = 'Nenhuma informação foi enviada'
          return false
        end

        unless @amount.valid?
          @last_error = 'Valor do boleto inválido'
          return false
        end

        unless @billing_address.valid?
          @last_error = 'Endereço de cobrança inválido'
          return false
        end

        unless @shopper_name.valid?
          @last_error = 'Nome do cliente inválido'
          return false
        end

        unless @social_security_number =~ /^[\d]{11,14}$/
          @last_error = 'CPF/CNPJ inválido'
          return false
        end

        true
      end

    end
  end
end
