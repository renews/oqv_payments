require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Refund::Base do
  it { is_expected.to respond_to(:dispatch).with(0).arguments }
  it { expect{ described_class.new.dispatch }.to raise_error(OqvPayments::OqvPaymentError) }
end
