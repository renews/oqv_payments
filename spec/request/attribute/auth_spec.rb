require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::Auth do
  let(:auth) { described_class.new({user: 'test', pass: 'secret'}) }

  subject{ auth }

  it { is_expected.to respond_to :user }
  it { is_expected.to respond_to :pass }

  it 'should set currency to a valid string' do
    expect(auth.user).to be_a_kind_of(String)

    auth.user = ''
    expect(auth.valid?).to be_falsy
  end

  it 'should set currency to a valid string' do
    expect(auth.pass).to be_a_kind_of(String)

    auth.pass = ''
    expect(auth.valid?).to be_falsy
  end

  it "should respond to valid" do
    expect(auth.valid?).to be_truthy
  end
end
