require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::BillingAddress do
  let(:billing_address) { described_class.new({city: 'São Paulo', country: 'BR', house_number_or_name: '999', postal_code: '04787910', state_or_province: 'SP', street: 'Roque Petroni Jr'}) }

  subject { billing_address }

  it { is_expected.to respond_to :city }
  it { is_expected.to respond_to :country }
  it { is_expected.to respond_to :house_number_or_name }
  it { is_expected.to respond_to :postal_code }
  it { is_expected.to respond_to :state_or_province }
  it { is_expected.to respond_to :street }

  it "should respond to valid" do
    expect(billing_address.valid?).to be_truthy
  end

end