require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::Card do
  it { is_expected.to respond_to(:to_json).with(0).arguments }
  it { is_expected.to respond_to(:as_json).with(0).arguments }

  describe 'having a valid serialized output' do
    let(:card) { described_class.new }
    subject { card.as_json }

    it { is_expected.to have_key(:number) }
    it { is_expected.to have_key(:expiryMonth) }
    it { is_expected.to have_key(:expiryYear) }
    it { is_expected.to have_key(:cvc) }
    it { is_expected.to have_key(:holderName) }
  end

  context 'when validating a card' do
    let(:card) {
      described_class.new(
        number: '4111111111111111',
        expiry_month: DateTime.now.month + 1,
        expiry_year: DateTime.now.year + 1,
        cvc: 123,
        holder_name: 'Foo'
      )
    }

    it 'must reject invalid card number' do
      card.number = 'yay'
      expect(card.valid?).to be_falsy

      card.number = '411111111111111'
      expect(card.valid?).to be_falsy

      card.number = '41111111111111123'
      expect(card.valid?).to be_falsy
    end

    it 'must reject invalid cvc' do
      card.cvc = ''
      expect(card.valid?).to be_falsy

      card.cvc = '1234'
      expect(card.valid?).to be_falsy

      card.cvc = '12'
      expect(card.valid?).to be_falsy
    end

    it 'must reject invalid holder name' do
      card.holder_name = ''
      expect(card.valid?).to be_falsy
    end

    it 'must reject invalid card expirity month' do
      card.expiry_month = 13
      expect(card.valid?).to be_falsy

      card.expiry_month = 0
      expect(card.valid?).to be_falsy
    end

    it 'must reject invalid card expirity year' do
      card.expiry_year = 'yay'
      expect(card.valid?).to be_falsy

      card.expiry_year = DateTime.now.year - 1
      expect(card.valid?).to be_falsy
    end
  end
end
