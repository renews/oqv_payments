require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::DueDate do
  let(:delivery_date) { described_class.new(3) }

  subject { delivery_date }

  it { is_expected.to respond_to :date }

  it "should respond to valid" do
    expect(delivery_date.valid?).to be_truthy
  end
end