require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::GenericAddress do
  generic_address_params = {
    street: 'street',
    house_number_or_name: 'house_number_or_name',
    city: 'city',
    postal_code: 'postal_code',
    state_or_province: 'state_or_province',
    country: 'country'
  }

  it { is_expected.to respond_to :street }
  it { is_expected.to respond_to :house_number_or_name }
  it { is_expected.to respond_to :city }
  it { is_expected.to respond_to :postal_code }
  it { is_expected.to respond_to :state_or_province }
  it { is_expected.to respond_to :country }

  context 'when having a valid serialized ooutput' do
    subject { described_class.new(generic_address_params).as_json }

    it { is_expected.to have_key(:street) }
    it { is_expected.to have_key(:houseNumberOrName) }
    it { is_expected.to have_key(:city) }
    it { is_expected.to have_key(:postalCode) }
    it { is_expected.to have_key(:stateOrProvince) }
    it { is_expected.to have_key(:country) }

    describe 'and values' do
      it { expect(subject[:street]).to eql(generic_address_params[:street])  }
      it { expect(subject[:houseNumberOrName]).to eql(generic_address_params[:house_number_or_name])  }
      it { expect(subject[:city]).to eql(generic_address_params[:city])  }
      it { expect(subject[:postalCode]).to eql(generic_address_params[:postal_code])  }
      it { expect(subject[:stateOrProvince]).to eql(generic_address_params[:state_or_province])  }
      it { expect(subject[:country]).to eql(generic_address_params[:country])  }
    end
  end
end
