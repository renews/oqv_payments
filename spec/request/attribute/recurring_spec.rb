require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::Recurring do
  let(:recurring) { described_class.new({contract: 'RECURRING'}) }

  subject{ recurring }

  it { is_expected.to respond_to :contract }

  it 'should set currency to a valid string' do
    expect(recurring.contract).to be_a_kind_of(String)

    recurring.contract = ''
    expect(recurring.valid?).to be_falsy
  end

  it "should respond to valid" do
    expect(recurring.valid?).to be_truthy
  end
end
