require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Attribute::ShopperName do
  let(:shopper_name) { described_class.new({first_name: 'John', last_name: 'Smith'}) }

  subject { shopper_name }

  it { is_expected.to respond_to :first_name }
  it { is_expected.to respond_to :last_name }

  it "should respond to valid" do
    expect(shopper_name.valid?).to be_truthy
  end
end