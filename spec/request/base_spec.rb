require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::Base do
  it { is_expected.to respond_to(:dispatch).with(0).arguments }
end
