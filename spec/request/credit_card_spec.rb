require 'spec_helper'
require 'oqv_payments'

describe OqvPayments::Request::CreditCard do
  it { is_expected.to respond_to(:to_json).with(0).arguments }
  it { is_expected.to respond_to(:as_json).with(0).arguments }
  
  context 'having a valid serialized output' do
    let(:request) { described_class.new additional_data: { card_encrypted_json: '' } }
    subject { request.as_json }

    it { is_expected.to have_key(:amount) }
    it { is_expected.to have_key(:reference) }
    it { is_expected.to have_key(:merchantAccount) }
    it { is_expected.to have_key(:shopperEmail) }
    it { is_expected.to have_key(:shopperReference) }
    it { is_expected.to have_key(:additionalData) }
  end

  context 'with risk system' do
    let(:request_with_risk_analisys) { described_class.new delivery_address: { }, billing_address: { }, shopper: { } }
    subject { request_with_risk_analisys.as_json }

    it { is_expected.to have_key(:deliveryAddress) }
    it { is_expected.to have_key(:billingAddress) }
    it { is_expected.to have_key(:shopper) }
    it { is_expected.to have_key(:socialSecurityNumber) }
  end

  context 'check for valid data' do
    let(:request) { described_class.new }
    subject { request }

    it { expect{ request.dispatch }.to raise_error(OqvPayments::OqvPaymentError) }
  end

  describe 'when using easy encrypt' do
    let(:request) {
        described_class.new({
          amount: {
            value: 100,
            currency: "BRL"
          },
          installments: {
            value: 3
          },
          reference: "test-613615",
          additional_data: {
            card_encrypted_json: "adyenjs_0_1_17$EfSUNYmtUE4BqaZ5v4h91EvXCUwINX9yjO54HCExwNiBholMFz+YVRLpCQlyNHvdE8br2Q1eHSBwVMYzfUr9h0kKYOoC/t7KEvKa9rgHjOnocPqsMUEJDRoRs8WUQkHzjhfK5Ib1Lq4XX3AfsMBhfukxEWaiI66hgk1wOu/K1EXHFI7yUry2WiALY2d1Wmb/CLhkt4rj7pwX6ooEpjMJSkSOQHS365tkHN/BsmESoEt5fXmRDP1iW1qW3Aj9FwKttBtIfzVFIczcfgqLLHMCYYS0bpJy1tcZxxhw9IiGDGUOncG3jK0uklRPeKNnYE8oT18kGme8VSrMNwclSVy4dA==$WlQRGSuic56mGOYcb7qy9U08ZqHqVgcp+dXf71Qdmt26wGbPFEvZ/oVvA2HcQPAWsCbP6Qt4STm30i5QYZhCOPwS2WwojkzC7fVv3ca/TICyF9BTjEGjVl6VFDE6YVx1gOTdenFemJbYbrwwwrdphkeMz+4MMLvV83I2cp1XkGLx7uP+Oz/jJzV4M/Zju9RdCukHin8T1QV6nsJbDW//KajdFFiHT73V/ATe2c9grosFr6ZJwG33s/zY8HEp92UA9vAXO2ceLUmcj6fbuWI/DNBjmCo1+R/J3+HnpNoXTw2W+CrCzNRBtZ8HsUd7Th21/H/O1SqO572A"
          }
        }
      )
    }

    it { expect(request.valid?).to be_truthy }

    it "should return 200" do
      response = request.dispatch
      expect(response['status']).to eq(200)
    end
  end

  describe 'when using avs' do
    let(:request) do
      described_class.new(
        {
          card: {
            number: "4111111111111111",
            expiry_month: "6",
            expiry_year: "2016",
            cvc: "737",
            holder_name: "teste ti"
          },
          amount: {
            value: "100",
            currency: "BRL"
          },
          installments: {
            value: 3
          },
          reference: "teste",
          shopper_email: "ivan.birkman@oqvestir.com.br",
          shopper_reference: "pada1",
          recurring: { 
            contract: "RECURRING"
          }
        }
      )
    end

    it "should return 200" do
      response = request.dispatch
      expect(response['status']).to eq(200)
    end
  end
end
